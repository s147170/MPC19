clc;
clear;

a1 = 1.2272;
a2 = 1.2272;
a3 = 1.2272;
a4 = 1.2272;

A1 = 380.1327;
A2 = 380.1327;
A3 = 380.1327;
A4 = 380.1327;

gamma1 = 0.45;
gamma2 = 0.4;

g = 981;
rho = 1.00;

p = [a1;a2;a3;a4;A1;A2;A3;A4;gamma1;gamma2;g;rho];

u0 = [100,200,0,0];

[A,B,Cy,D,G,C,steadystate] = FourTankLinearized(u0,p);

FourTankSystem = ss(A,B,Cy,D);

%%

x0 = [0,0,0,0];
T = (0:1:20*60);

N = length(T);

U = 100*ones(2,N);
D = zeros(2,N);

Q = [5 0; 0 5];
Lq = chol(Q,'lower');
w = Lq*randn(2,N);

[Tsim,Y,X] = FourTankSimulation(T,x0,U,w,p);

figure(1)
plot(Tsim,X)

figure(2)
lsim(FourTankSystem,U,T)


%%




