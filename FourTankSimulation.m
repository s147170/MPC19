function [Tx,Y,X] = FourTankSimulation(T,x0,U,D,v,p)
%Four Tank Simulation: returns system output, states and time vector for
%simulation of the Four Tank System
%     [Tx,Y,X] = FourTankSimulation(T,x0,U,D,p)
%     x = [m1,m3,m3,m4]: States
%     u = [F1,F2]: Inputs
%     D = [F3,F4]: Disturbances
%     p = [a1,a2,a3,a4,A1,A2,A3,A4,gamma1,gamma2,g,rho]: Parameters

options = odeset('RelTol',1e-6,'AbsTol',1e-6);
N = length(T);

Tx = T(1);
X = x0;
Y = FourTankSystemOutput(X,p);

for k=1:N-1    
    x = X(end,:);
    
    [Tk,Xk] = ode45(@FourTankDynamics,[T(k) T(k+1)],x,options,U(:,k),D(:,k),p);
    X = [X; Xk];
    
    Yk = FourTankSystemOutput(Xk,p) + v(k);
    
    Y = [Y; Yk];
    Tx = [Tx; Tk];
end
end

