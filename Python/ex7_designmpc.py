from MPCTools import *
from FourTankSystem import *
from scipy.optimize import fsolve
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.linalg import solve_discrete_are

np.set_printoptions(precision=4)

# --- PARAMETERS --- #
nu = 2  # inputs
nd = 2  # unmeasured disturbances
nx = 4  # number of states
ny = 4  # measurements
nz = 2  # outputs

at = np.ones(nx) * 1.2272
At = np.ones(nx) * 380.1327

gt = np.array([0.65, 0.6])
xs0 = np.ones([1, nx]) * 1e3
us = np.array([[250], [325]])

D = np.array([[0, 0], [0, 0]])

# --- LINEARIZE AND DISCRETIZE --- #
xs = fsolve(fourTankPlantWrap, xs0, (np.squeeze(us), at, At, gt))
A, B, C, Cz = linearizeFourTank(xs, us, at, At, gt)
print("Cz:\n", Cz)
Ts = 10
Abar, Bbar = c2dzoh(A, B, Ts)

# --- MPC DESIGN  --- #
# weights
ph = 20  # prediction horizon
Qz = np.array([[20 ** 2, 0], [0, 10 ** 2]])  # output weights
S = np.eye(nu) * 1e-4  # input weight !!!!

# Kalman filter design
G = np.r_[np.eye(nd), np.zeros((nx - nd, nd))]
print("G: \n", G)
R_ww = np.eye(nd)   # process noise
R_vv = np.eye(ny) * 1e2  # measureemnt noise
R_vw = np.zeros((ny, nd))  # Covariance v influence on w
R_wv = R_vw.T  # Covariance w influence on v

P = solve_discrete_are(Abar.T, C.T, G @ R_ww @ G.T, R_vv)  # , G @ R_wv)
R_e_inv = np.linalg.inv(C @ P @ C.T + R_vv)
K_fx = P @ C @ R_e_inv
K_fw = R_wv @ R_e_inv

mpc = MPC(ph, Abar, Bbar, C, Cz, xs=xs, us=us, Qz=Qz, S=S)
print("Phi: ", mpc.phi)
print("Gamma: ", mpc.gamma)
print("Lx0: ", mpc.L_x0)
print("LR: ", mpc.L_R)
mpc.Lim.u = np.array([[0., 500.], [0., 500.]])
mpc.Lim.du = np.array([[-20., 20.], [-20., 20.]])
mpc.Lim.z = np.array([[0., 60.], [0., 50.]])


# --- SIMULATE --- #
t0 = 0
tf = 10 * 60
t = np.linspace(0, tf, tf / Ts)
N = len(t)

x = np.zeros((N, nx))
z = np.zeros((N, nz))
u = np.zeros((N, nu))
y = np.zeros((N, ny))
w_hat = np.zeros((N, nd))

x[0, :] = mpc.xs.T
z[0, :] = mpc.zs.T
print("z_s: ", mpc.zs)

r = np.tile(np.array([[55.], [35.21]]), (ph, 1))
print("zs matrix: \n", np.tile(mpc.zs, (mpc.ph, 1)))
print("r matrix: \n", r)


for k in range(0, N - 1):
    # compute future step
    u[k, :] = mpc.mpcCompute(r)

    # get measurement
    sol = odeint(fourTankPlant, x[k, :], t[k:k + 2],
                 args=(u[k, :], at, At, gt),
                 tfirst=True)
    y[k, :] = fourTankSensor(sol[-1, :], at, At, gt)

    # get model estimate
    x_hat = np.squeeze(mpc.A @ mpc.X + mpc.B @ mpc.u_m1) + G @ w_hat[k, :]
    y_hat = mpc.C @ x_hat
    e = y[k, :] - y_hat
    mpc.X = np.reshape(x_hat.squeeze() + K_fx @ e, (mpc.nx, 1))
    w_hat[k + 1, :] = (K_fw @ e).T

    x[k + 1, :] = (mpc.X + mpc.xs).T
    z[k + 1, :] = np.squeeze(mpc.output())

u[-1, :] = u[-2, :]
y[-1, :] = y[-2, :]
print("\nFinal z: ", z[-1, :], "Final y: ", y[-1, :nz])

# plot
t_plot = t / 60
r_plot = np.tile(r[:nz].T, (N, 1))
plt.subplot(221)
plt.plot(t_plot, u[:, 0], 'r')
plt.ylabel('u1')
plt.ylim(0, 600)
plt.subplot(222)
plt.plot(t_plot, u[:, 1], 'r')
plt.ylabel('u2')
plt.ylim(0, 600)

plt.subplot(223)
plt.plot(t_plot, z[:, 0], 'm', t_plot, y[:, 0], 'b', t_plot, r_plot[:, 0], 'k')
plt.ylabel('z1')
plt.ylim(0, 100)
plt.subplot(224)
plt.plot(t_plot, z[:, 1], 'm', t_plot, y[:, 1], 'b', t_plot, r_plot[:, 1], 'k')
plt.ylabel('z2')
plt.ylim(0, 100)
# plt.figure()
# plt.plot(t_plot, x)
# plt.ylabel('x')
# plt.ylim(0, 15000)

plt.show()
