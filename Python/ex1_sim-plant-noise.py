from scipy.integrate import odeint
import matplotlib.pyplot as plt

from FourTankSystem import *

# parameters
nu = 2
nx = 4
ny = 4
nz = 2

a = np.ones(nx) * 1.2272
A = np.ones(nx) * 380.1327

gamma = np.array([0.45, 0.4])
rho = 1

# simulation parameters
t0 = 0
tf = 20 * 60
t = np.linspace(0, tf, 200)
N = len(t)

m0 = np.ones([1, nx]) * 1e3
F = np.array([[300, 300]])
x0 = m0
u = np.repeat(F, N, axis=0)  # constant input

# process noise
Q = np.array([[20 ** 2, 0], [0, 40 ** 2]])
Lq = np.linalg.cholesky(Q)
w = np.transpose(Lq @ np.random.rand(nu, N))

# measurement noise
R = np.eye(ny)
Lr = np.linalg.cholesky(R)
v = np.transpose(Lr @ np.random.rand(ny, N))


# loop through each time step
x = np.zeros((N, nx))
y = np.zeros((N, ny))
z = np.zeros((N, nz))
X = np.zeros((N, nx))

x[0, :] = x0

# plt.hist(w[:, 0])
# plt.show()

for k in range(0, N - 1):
  y[k, :] = fourTankSensor(x[k, :], a, A, gamma) + v[k, :]
  z[k, :] = fourTankOutput(x[k, :], a, A, gamma)

  sol = odeint(fourTankPlant, x[k, :], t[k:k + 2],
               args=(u[k, :] + w[k, :], a, A, gamma),
               tfirst=True)

  x[k + 1, :] = sol[-1, :]


# plot
t_plot = t / 60
plt.subplot(411)
plt.plot(t_plot, y[:, 0])
plt.ylabel('h1')
plt.subplot(412)
plt.plot(t_plot, y[:, 1])
plt.ylabel('h2')
plt.subplot(413)
plt.plot(t_plot, y[:, 2])
plt.ylabel('h3')
plt.subplot(414)
plt.plot(t_plot, y[:, 3])
plt.ylabel('h4')
plt.show()
