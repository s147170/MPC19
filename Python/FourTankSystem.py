import numpy as np
import sympy as sp
# from MPCTools import *


def fourTankPlant(t, x, u, a, A, gamma, g=981, rho=1):
    # Implements fourtank system differential model
    # dx/dt = f(t, x, u, a, A, gamma)
    #
    # inputs assumed to be numpy arrays

    # setup
    m = x
    F = u

    # inflows
    qin = np.zeros(4)
    qin[0] = gamma[0] * F[0]
    qin[1] = gamma[1] * F[1]
    qin[2] = (1 - gamma[1]) * F[1]
    qin[3] = (1 - gamma[0]) * F[0]

    # outflows
    h = m / (rho * A)
    qout = a * np.sqrt(2 * g * h)
    # print(f'Q_in: {qin}, Q_out: {qout}')

    # Differential equations
    xdot = np.zeros(4)
    xdot[0] = rho * (qin[0] + qout[2] - qout[0])
    xdot[1] = rho * (qin[1] + qout[3] - qout[1])
    xdot[2] = rho * (qin[2] - qout[2])
    xdot[3] = rho * (qin[3] - qout[3])

    return xdot


def fourTankPlantDist(t, x, u, d, a, A, gamma, g=981, rho=1, R_ww=np.eye(2)):
    # Implements fourtank system differential model
    # dx/dt = f(t, x, u, d, a, A, gamma)
    #
    # inputs assumed to be numpy arrays

    # setup
    m = x
    F = u
    Fd = R_ww @ np.random.randn(2, 1)

    # inflows
    qin = np.zeros(4)
    qin[0] = gamma[0] * F[0]
    qin[1] = gamma[1] * F[1]
    qin[2] = (1 - gamma[1]) * F[1] + Fd[0]
    qin[3] = (1 - gamma[0]) * F[0] + Fd[1]

    # outflows
    h = m / (rho * A)
    qout = a * np.sqrt(2 * g * h)
    # print(f'Q_in: {qin}, Q_out: {qout}')

    # Differential equations
    xdot = np.zeros(4)
    xdot[0] = rho * (qin[0] + qout[2] - qout[0])
    xdot[1] = rho * (qin[1] + qout[3] - qout[1])
    xdot[2] = rho * (qin[2] - qout[2])
    xdot[3] = rho * (qin[3] - qout[3])

    return xdot


def fourTankPlantWrap(x, u, a, A, gamma, g=981, rho=1):
    return fourTankPlant(0, x, u, a, A, gamma, g, rho)


def fourTankSensor(x, a, A, gamma, g=981, rho=1):
    y = x / (rho * A)
    return y


def fourTankOutput(x, a, A, gamma, g=981, rho=1):
    return x[0:2] / (rho * A[0:2])


def fourTankPlantSympy(t, x, u, a, A, gamma, g=981, rho=1):
    # Implements fourtank system differential model
    # dx/dt = f(t, x, u, a, A, gamma)
    #
    # inputs assumed to be numpy arrays

    # setup
    m = x
    F = u

    # inflows
    qin = sp.zeros(4, 1)
    qin[0] = gamma[0] * F[0]
    qin[1] = gamma[1] * F[1]
    qin[2] = (1 - gamma[1]) * F[1]
    qin[3] = (1 - gamma[0]) * F[0]

    # outflows
    h = sp.zeros(4, 1)
    qout = sp.zeros(4, 1)
    for n in range(len(A)):
        h[n] = m[n] / (rho * A[n])
        qout[n] = a[n] * sp.sqrt(2 * g * h[n])
    # print(f'Q_in: {qin}, Q_out: {qout}')

    # Differential equations
    xdot = sp.zeros(4, 1)
    xdot[0] = rho * (qin[0] + qout[2] - qout[0])
    xdot[1] = rho * (qin[1] + qout[3] - qout[1])
    xdot[2] = rho * (qin[2] - qout[2])
    xdot[3] = rho * (qin[3] - qout[3])

    return xdot


def fourTankSensorSympy(x, a, A, gamma, g=981, rho=1):
    y = sp.zeros(4, 1)
    for n in range(len(y)):
        y[n] = x[n] / (rho * A[n])
    return y


def linearizeFourTank(xs, us, at, At, gamma, verbose=False):

    # Linearize around steady state
    x1, x2, x3, x4 = sp.symbols('x1 x2 x3 x4')
    u1, u2 = sp.symbols('u1 u2')
    x = sp.Matrix([x1, x2, x3, x4]).transpose()
    u = sp.Matrix([u1, u2]).transpose()

    f1 = fourTankPlantSympy(0, x, u, at, At, gamma)

    A = f1.jacobian(x).subs({x1: xs[0], x2: xs[1], x3: xs[2], x4: xs[3]})
    B = f1.jacobian(u).subs({u1: us[0], u2: us[1]})

    g = fourTankSensorSympy(x, at, At, gamma)
    # print(g)
    C = g.jacobian(x).subs({x1: xs[0], x2: xs[1], x3: xs[2], x4: xs[3]})
    Cz = g.jacobian(x[:2]).subs({x1: xs[0], x2: xs[1]})

    sys = (np.array(A).astype(np.float),
           np.array(B).astype(np.float),
           np.array(C).astype(np.float),
           np.array(Cz).astype(np.float).transpose())
    if verbose:
        print("Linearized A: ", A)
        print("Linearized B: ", B)
        print("Linearized C: ", C)
        print("Linearized Cz: ", Cz)
    return sys
