import inspect
import numpy as np
from scipy.linalg import expm
import quadprog as qp
# from scipy.optimize import fsolve
# from scipy.integrate import odeint
# import matplotlib.pyplot as plt


def stateSpaceGeneric(t, x, u, A, B, C, D):
    # print("Size A: ", A.shape, ", x: ", x.shape,
    #       ", B: ", B.shape, ", u:", u.shape)
    xdot = A @ x + B @ u
    return xdot


def outputFuncGeneric(x, u, A, B, C, D):
    y = C @ x + D @ u
    return y


def c2dzoh(A, B, Ts, verbose=False):
    nx, nu = B.shape
    AB = np.concatenate((A, B), axis=1)
    M = np.concatenate((AB, np.zeros((nu, nx + nu))), axis=0)
    Phi = expm(M * Ts)

    Abar = Phi[:nx, :nx]
    Bbar = Phi[:nx, nx:]

    if verbose:
        # print(M)
        # print()
        # print(Phi)
        print("\nDiscretized A: ", Abar)
        print("Discretized B: ", Bbar)

    return (Abar, Bbar)


def buildPhiMatrix(ph, A, Cz):
    nz, nx = Cz.shape
    phi = np.zeros((nz * ph, nx))
    for n in range(ph):
        phi[n * nz: (n + 1) * nz, :] = Cz @ np.linalg.matrix_power(A, n + 1)

    return phi


def buildGammaMatrix(ph, A, B, Cz):
    nu = B.shape[1]
    nz = Cz.shape[0]
    gamma = np.zeros((nz * ph, nu * ph))
    H = []
    for i in range(ph):
        H.append(Cz @ np.linalg.matrix_power(A, i) @ B)
        for j in range(i + 1):
            gamma[nz * i: nz * (i + 1), nu * j: nu * (j + 1)] = H[i - j]

    return gamma


def build_HS_MU_Matrix(ph, S):
    nu = S.shape[0]

    # Build H_S, a ph * nu x ph * nu matrix.
    # It should have 2 * S in the diagonal except for the final matrix which should just be S
    # additionally, it should be padded with -S on left and right side of the diagonal
    H_S = np.zeros((ph * nu, ph * nu))
    H_S[:nu, :nu] = 2 * S
    H_S[:nu, nu:nu * 2] = -S
    for i in range(1, ph - 1):
        H_S[nu * i:nu * (i + 1), nu * (i - 1):nu * i] = -S
        H_S[nu * i:nu * (i + 1), nu * i:nu * (i + 1)] = 2 * S
        H_S[nu * i:nu * (i + 1), nu * (i + 1):nu * (i + 2)] = -S
    H_S[-nu:, -nu * 2:-nu] = -S
    H_S[-nu:, -nu:] = S

    # M_u1 - ph * nu x nu matrix, with the first element being equal to S
    M_u1 = np.zeros((ph * nu, nu))
    M_u1[:nu, :nu] = S

    return (H_S, M_u1)


def buildDeltaMatrix(ph, nu):
    Delta_row1 = np.c_[np.eye(nu), np.zeros((nu, nu * (ph - 1)))]
    Delta_rows = np.zeros(((ph - 1) * nu, ph * nu))
    for n in range(ph - 1):
        Delta_rows[n * nu:(n + 1) * nu, n * nu:(n + 1) * nu] = -np.eye(nu)
        Delta_rows[n * nu:(n + 1) * nu, (n + 1) * nu:(n + 2) * nu] = np.eye(nu)

    return np.r_[Delta_row1, Delta_rows]


class MPCLimits(object):

    def __init__(self, ph, nu, nz, u=None, du=None, z=None, z_soft=None):
        self.ph = ph
        self.nu = nu
        self.nz = nz

        if u is None:
            u = np.tile(np.array([-np.inf, np.inf]), (nu, 1))
        self.u = u

        if du is None:
            du = np.tile(np.array([-np.inf, np.inf]), (nu, 1))
        self.du = du

        if z is None:
            z = np.tile(np.array([-np.inf, np.inf]), (nz, 1))
        self.z = z

        if z_soft is None:
            z_soft = np.tile(np.array([-np.inf, np.inf]), (nz, 1))
        self.z_soft = z_soft

    def update(self):
        # manually re-set all limits to trigger resize
        argnames = ['u', 'du', 'z', 'z_soft']
        for var in argnames:
            setattr(self, var, getattr(self, var))

    @property
    def u(self):
        return self._u

    @u.setter
    def u(self, value):
        if value.shape[0] != self.nu * self.ph:
            self._u = np.tile(value[:self.nu, :], (self.ph, 1))
        else:
            self._u = value

    @property
    def du(self):
        return self._duu

    @du.setter
    def du(self, value):
        if value.shape[0] != self.nu * self.ph:
            self._duu = np.tile(value[:self.nu, :], (self.ph, 1))
        else:
            self._duu = value

    @property
    def z(self):
        return self._z

    @z.setter
    def z(self, value):
        if value.shape[0] != self.nz * self.ph:
            self._z = np.tile(value[:self.nz, :], (self.ph, 1))
        else:
            self._z = value

    @property
    def z_soft(self):
        return self._z_soft

    @z_soft.setter
    def z_soft(self, value):
        if value.shape[0] != self.nz * self.ph:
            self._z_soft = np.tile(value[:self.nz, :], (self.ph, 1))
        else:
            self._z_soft = value


class MPC:

    def __init__(self, ph, A, B, C, Cz, D=None, E=None, xs=None, us=None, es=None, Qz=None, S=None):
        # basic properties
        self.nx, self.nu = B.shape
        self.nz = Cz.shape[0]

        self.ph = ph
        self.A = A
        self.B = B
        self.C = C
        self.Cz = Cz
        if D is None:
            D = np.zeros((self.nz, self.nu))
        self.D = D
        self.E = E

        if Qz is None:
            Qz = np.eye(self.nz)
        self.Qz = Qz

        if S is None:
            S = np.eye(self.nu)
        self.S = S

        # state etc
        self.X = np.zeros((self.nx, 1))
        if xs is None:
            xs = np.zeros((self.nx, 1))
        self.xs = np.reshape(xs, (self.nx, 1))
        if us is None:
            us = np.zeros((self.nu, 1))
        self.us = np.reshape(us, (self.nu, 1))
        self.es = es
        self.u_m1 = np.zeros((self.nu, 1))  # u_(-1)

        self.designMPC()
        self.zs = np.reshape(self.output(), (self.nz, 1))

        # default limits
        self.Lim = MPCLimits(self.ph, self.nu, self.nz)

    def designMPC(self):
        # Calculate matrices for MPC system of certain prediction horizon
        # This assumes discrete state space LTI model of a system, and uses the formulation:
        #
        #   Z = phi * x0 + gamma * U
        #
        # where Z is a vector of nz * ph measured outputs, x0 is the initial state,
        # and U is a vector of nu * ph input variables. The matrices calculated are for use with solving the
        # QP problem, which essentially minimizes the LS problem R - Z by finding an optimal U.

        # build phi and gamma matrices
        self.phi = buildPhiMatrix(self.ph, self.A, self.Cz)
        self.gamma = buildGammaMatrix(self.ph, self.A, self.B, self.Cz)

        # Qz, H_S and H matrices
        self.Qz_cal = np.kron(np.eye(self.ph), self.Qz)
        H_S, self.M_u1 = build_HS_MU_Matrix(self.ph, self.S)
        print("H_S: \n", H_S)
        self.H = self.gamma.T @ self.Qz_cal @ self.gamma + H_S

        # M matrices used to calculate g for LS formulation
        self.M_x0 = self.gamma.T @ self.Qz_cal @ self.phi
        self.M_R = - self.gamma.T @ self.Qz_cal
        if self.E is not None:
            self.gamma_d = buildGammaMatrix(self.ph, self.A, self.E, self.Cz)
            self.M_D = self.gamma.T @ self.Qz_cal @ self.gamma_d
        else:
            self.M_D = np.array([0])
            self.gamma_d = np.array([0])

        # calculate LS gains for directly solving the LS problem w/o constraints (explicit MPC)
        H_chol = np.linalg.cholesky(self.H).T
        self.L_x0 = - np.linalg.solve(H_chol, np.linalg.solve(H_chol.T, self.M_x0))
        self.L_R = - np.linalg.solve(H_chol, np.linalg.solve(H_chol.T, self.M_R))
        self.L_u1 = - np.linalg.solve(H_chol, np.linalg.solve(H_chol.T, self.M_u1))

        # Delta matrix used for rate limits
        self.Delta = buildDeltaMatrix(self.ph, self.nu)

        # kalman filter constants

    def mpcCompute(self, r, d=None, verbose=False):
        # convert from physical variables to deviation variables
        R = r - np.tile(self.zs, (self.ph, 1))
        if d is not None:
            D = d - self.es
        else:
            D = np.array([0])

        # setup input and input rate constraints
        U_lim = self.Lim.u - np.tile(self.us, (self.ph, self.nu))
        u_lim_1 = np.c_[self.u_m1, self.u_m1]
        dU_lim = self.Lim.du + np.r_[u_lim_1, np.zeros(((self.ph - 1) * self.nu, self.nu))]
        Au = np.r_[np.eye(self.nu * self.ph), -np.eye(self.nu * self.ph),
                   self.Delta, -self.Delta]
        bu = np.r_[U_lim[:, 0], -U_lim[:, 1], dU_lim[:, 0], -dU_lim[:, 1]]

        # output hard constraints
        Z_lim = self.Lim.z - np.tile(self.zs, (self.ph, self.nz))

        Az = np.r_[self.gamma, -self.gamma]
        phi_x0 = np.squeeze(self.phi @ self.X)
        bz = np.r_[Z_lim[:, 0] - phi_x0 - self.gamma_d @ D,
                   -(Z_lim[:, 1] - phi_x0 - self.gamma_d @ D)]

        if verbose:
            print(dU_lim)

        # calculate optimal u
        g = self.M_x0 @ self.X + self.M_R @ R + self.M_u1 @ self.u_m1 + self.M_D * D
        out = qp.solve_qp(self.H, -np.squeeze(g), np.r_[Au, Az].T, np.r_[bu, bz])
        U = out[0]
        U_0 = np.reshape(U[:self.nu], (self.nu, 1))
        # U_0 = self.L_x0[:self.nu, :] @ self.X + self.L_R[:self.nu, :]
        #@ R + self.L_u1[:self.nu, :] @ self.u_m1

        # convert back to physical variables
        self.u_m1 = U_0
        u_0 = U_0 + self.us
        return np.squeeze(u_0)

    def output(self):
        return self.Cz @ (self.X + self.xs)
