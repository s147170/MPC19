from scipy.optimize import fsolve
from scipy.integrate import odeint
import matplotlib.pyplot as plt

from MPCTools import *
from FourTankSystem import *

# parameters
nu = 2
nx = 4
ny = 4
nz = 2

at = np.ones(nx) * 1.2272
At = np.ones(nx) * 380.1327

gamma = np.array([0.45, 0.4])
xs0 = np.ones([1, nx]) * 1e3
us = np.array([250, 325])

D = np.array([[0, 0], [0, 0]])

# Find steady state point
xs = fsolve(fourTankPlantWrap, xs0, (np.squeeze(us), at, At, gamma))
print("Steady state x:", xs)

# linearize around steady state
A, B, C, Cz = linearizeFourTank(xs, us, at, At, gamma)

# discretize using Zero-order-hold:
Ts = 10
Abar, Bbar = c2dzoh(A, B, Ts)

# --- SIMULATE ---
# simulation parameters
t0 = 0
tf = 20 * 60
t = np.linspace(0, tf, 200)
N = len(t)
xs_vec = np.repeat(xs[np.newaxis, :], N, axis=0)

u_const = np.repeat(us[np.newaxis, :], N, axis=0)  # constant input
u = []
u.append(np.stack((u_const[:, 0] * 1.05, u_const[:, 1]), axis=-1))
u.append(np.stack((u_const[:, 0] * 1.1, u_const[:, 1]), axis=-1))
u.append(np.stack((u_const[:, 0] * 1.25, u_const[:, 1]), axis=-1))


# init plots
ax = []
ylims = np.array([[12000, 15000], [9000, 12000], [3500, 6500], [2000, 5000]])
for n in range(nx):
    ax.append(plt.subplot(411 + n))
    ax[n].set_ylabel("m%d" % (n + 1))
    ax[n].set_ylim(ylims[n, 0], ylims[n, 1])

# simulate and plot
X_nl = []
X_l = []
X_ld = []
sol_d = np.zeros((N, nx))

# for each set of inputs!
for n in range(len(u)):
    # nonlinear model
    sol = odeint(fourTankPlant, xs, t,
                 args=(u[n][0, :], at, At, gamma), tfirst=True)
    X_nl.append(sol)

    # linear model
    sol = odeint(stateSpaceGeneric, np.array([0, 0, 0, 0]), t,
                 args=(u[n][0, :] - u_const[0, :], A, B, C, D), tfirst=True)
    X_l.append(sol + xs_vec)

    # linear discretized model
    for k in range(N - 1):
        sol_d[k + 1, :] = Abar @ sol_d[k, :] + Bbar @ (u[n][k, :] - us)
    X_ld.append(sol_d + xs_vec)

    # plot!
    for p in range(nx):
        ax[p].plot(t, X_nl[n][:, p], 'r',
                   t, X_l[n][:, p], 'b',
                   t, X_ld[n][:, p], 'g')
plt.show()
