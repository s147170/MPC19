from scipy.integrate import odeint
import matplotlib.pyplot as plt

from FourTankSystem import *

# parameters
a = np.ones(4) * 1.2272
A = np.ones(4) * 380.1327

gamma = np.array([0.65, 0.6])
rho = 1

# simulation parameters
t0 = 0
tf = 20 * 60
t = np.linspace(0, tf, 1000)

m0 = np.ones(4) * 1e3
F = np.array([300, 300])
x0 = m0
u = F

sol = odeint(fourTankPlant, x0, t, args=(u, a, A, gamma), tfirst=True)
# h = sol / (rho * A)

# plot
t_plot = np.linspace(0, tf / 60, 1000)
plt.subplot(411)
plt.plot(t_plot, sol[:, 0])
plt.ylabel('m1')
plt.subplot(412)
plt.plot(t_plot, sol[:, 1])
plt.ylabel('m2')
plt.subplot(413)
plt.plot(t_plot, sol[:, 2])
plt.ylabel('m3')
plt.subplot(414)
plt.plot(t_plot, sol[:, 3])
plt.ylabel('m4')
plt.show()
