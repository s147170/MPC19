clc;
clear;

%% Problem 1 - System description

% 1.1 What are the states x, the measurement y, the manipulated variables
% (MVs) u, the measured disturbance variables (DVs) and the controlled
% variables (CVs) for this system?


% states: [m1,m2,m3,m4]                             %masses
% measurement: [h1,h2,h3,h4]                        %tank heights
% Measured disturbance variables: None ( the only two disturbance variables F3
% and F4 are mentioned as NOT measured)
% controlled variables: [h1,h2]
% controlled variables: [F1,F2]                     %flow rates
%% Deterministic Nonlinear Model

syms m1 m2 m3 m4 h1 h2 h3 h4 rho q1 q1in q2 q2in q3 q3in q4 q4in gamma1 gamma2 F1 F2 F3 F4 a1 a2 a3 a4 A1 A2 A3 A4 grav F10 F20 F30 F40

% Inflows
q1in = gamma1*F1;
q2in = gamma2*F2;
q3in = (1-gamma2)*F2;
q4in = (1-gamma1)*F1;

% Liquid heights
h1 = m1/(rho*A1);
h2 = m2/(rho*A2);
h3 = m3/(rho*A3);
h4 = m4/(rho*A4);

% Outflows
q1 = a1*sqrt(2*grav*h1);
q2 = a2*sqrt(2*grav*h2);
q3 = a3*sqrt(2*grav*h3);
q4 = a4*sqrt(2*grav*h4);

% 2.1 Develop a deterministic model for the dynamics of the system

f_deterministic = [
    rho*(q1in + q3 - q1)
    rho*(q2in + q4 - q2)    
    rho*(q3in - q3)
    rho*(q4in - q4)
];

% 2.2 Develop a mathematical model for the sensors (measurements)

g_deterministic = [
    m1/(rho*A1)
    m2/(rho*A2)
];

% 2.3 Develop a mathematical model for the outputs

h_deterministic = [
    m1/(rho*A1)
    m2/(rho*A2)
];

%% PARAMETERS

a1 = 1.2272;
a2 = 1.2272;
a3 = 1.2272;
a4 = 1.2272;

A1 = 380.1327;
A2 = 380.1327;
A3 = 380.1327;
A4 = 380.1327;

gamma1 = 0.45;
gamma2 = 0.40;

grav = 981;
rho = 1.00;

F10 = 300;
F20 = 300;
F30 = 250;
F40 = 250;

%% LINEAR STATE SPACE

u = [F1,F2,F3,F4];
u0 = [F10,F20,F30,F40];

x = [m1,m2,m3,m4];

[m10,m20,m30,m40] = solve(subs(f_deterministic,u,u0)==[0 0 0 0],[m1,m2,m3,m4]);
x0 = [m10,m20,m30,m40];

A = subs(simplify(jacobian(f_deterministic,x)),x,x0);

B = subs(simplify(jacobian(f,[F1,F2])),[F1,F2,F3,F4,m1,m2,m3,m4],[F10,F20,F30,F40,m10,m20,m30,m40]);

Cy = subs(simplify(jacobian(g,[m1,m2,m3,m4])),[F1,F2,F3,F4,m1,m2,m3,m4],[F10,F20,F30,F40,m10,m20,m30,m40]);

D = subs(simplify(jacobian(g,[F1,F2])),[F1,F2,F3,F4,m1,m2,m3,m4],[F10,F20,F30,F40,m10,m20,m30,m40]);

G = subs(simplify(jacobian(f,[F3,F4])),[F1,F2,F3,F4,m1,m2,m3,m4],[F10,F20,F30,F40,m10,m20,m30,m40]);

C = subs(simplify(jacobian(h,[m1,m2,m3,m4])),[F1,F2,F3,F4,m1,m2,m3,m4],[F10,F20,F30,F40,m10,m20,m30,m40]);

A = eval(subs(A));
B = eval(subs(B));
Cy = eval(subs(Cy));
D = eval(subs(D));
G = eval(subs(G));
C = eval(subs(C));

fourtanksystem = ss(A,B,Cy,D);

%% DISCRETE LINEAR STATE SPACE

Ts = 4;
T = (0:Ts:20*60);
s = 0.5;
U = [s*F10*ones(1,length(T));s*F20*ones(1,length(T));s*F30*ones(1,length(T));s*F40*ones(1,length(T))];

fourtankdiscrete = c2d(fourtanksystem,Ts,'zoh');

[y,T] = lsim(fourtankdiscrete,U,T);

ynorm = zeros(size(y));

ynorm(:,1) = y(:,1)./eval(subs(subs(m10),[F1,F2,F3,F4],[F10,F20,F30,F40]));
ynorm(:,2) = y(:,2)./eval(subs(subs(m20),[F1,F2,F3,F4],[F10,F20,F30,F40]));

figure(1)
plot(T./60,ynorm)
legend('h1','h2');

[Ad,Bd,Cd,Dd] = ssdata(fourtankdiscrete);

%% TRANSFER FUNCTION

fourtanktransfer = tf(fourtanksystem);

s = 0.1;
U = [s*F10;s*F20;s*F30;s*F40];

opt = stepDataOptions('InputOffset',0,'StepAmplitude',U);

[y,T] = step(fourtanktransfer,opt);

figure(1)
plot(T,y(:,1,1)./eval(subs(subs(m10),[F1,F2,F3,F4],[F10,F20,F30,F40])));

% Inserted something here


