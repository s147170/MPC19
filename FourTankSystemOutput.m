function z = FourTankSystemOutput(x,p)
%Four Tank Simulation: returns system output given states
%     z = FourTankSystemOutput(x,p)
%     x = [m1,m3,m3,m4]: States
%     p = [a1,a2,a3,a4,A1,A2,A3,A4,gamma1,gamma2,g,rho]: Parameters

    z = FourTankSystemSensor(x,p);
end