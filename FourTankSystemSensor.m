function y = FourTankSystemSensor(x,p)
%Four Tank Simulation: returns system sensor values given states
%     y = FourTankSystemSensor(x,p)
%     x = [m1,m3,m3,m4]: States
%     p = [a1,a2,a3,a4,A1,A2,A3,A4,gamma1,gamma2,g,rho]: Parameters

    rho = p(12);
    A = p(5:6)';
    y = x(:,1:2)./(rho*A);
end