function [u_k, I_kpp] = PID(uk,yk,y_k,y_kmm,I_k,K_P,K_I,K_D,dt,u_min,u_max)

e = yk - y;
P = K_P * e;
D = K_D * (y_k - y_kmm) / dt;
u_k = uk + P + I_k + D;

if (u_k >= u_max)
    u_k = umax;
elseif (u < u_min)
    u_k = umin;
else 
    I_kpp = I + K_I * e * dt;
end