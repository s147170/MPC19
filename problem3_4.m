clc;
clear
close all

a1 = 1.2272;
a2 = 1.2272;
a3 = 1.2272;
a4 = 1.2272;

A1 = 380.1327;
A2 = 380.1327;
A3 = 380.1327;
A4 = 380.1327;

gamma1 = 0.45;
gamma2 = 0.4;

g = 981;
rho = 1.00;

p = [a1;a2;a3;a4;A1;A2;A3;A4;gamma1;gamma2;g;rho];
fname = strcat(pwd,'\figs\problem3normalized') % Figure export directory

%% Problem 3.1 - without noise
close all;
u0 = [100,200,0,0];

x0 = [0,0,0,0];
T = (0:1:20*60);

N = length(T);

U = u0(1:2).*ones(N,2);
D = zeros(size(U));
v = zeros(N,2);

[Tsim0,Y0_,X0_] = FourTankSimulation(T,x0,U',D',v,p);
x0 = X0_(end,:)
[Tsim10,Y10_,X10_] = FourTankSimulation(T,x0,U'*1.1,D',v,p);
[Tsim25,Y25_,X25_] = FourTankSimulation(T,x0,U'*1.25,D',v,p);
[Tsim50,Y50_,X50_] = FourTankSimulation(T,x0,U'*1.5,D',v,p);

%Normalizing the two tank levels
Y10_1 = (Y10_(:,1)-Y10_(1,1))./(Y10_(end,1)-Y10_(1,1));
Y25_1 = (Y25_(:,1)-Y25_(1,1))./(Y25_(end,1)-Y25_(1,1));
Y50_1 = (Y50_(:,1)-Y50_(1,1))./(Y50_(end,1)-Y50_(1,1));

Y10_2 = (Y10_(:,2)-Y10_(1,2))./(Y10_(end,2)-Y10_(1,2));
Y25_2 = (Y25_(:,2)-Y25_(1,2))./(Y25_(end,2)-Y25_(1,2));
Y50_2 = (Y50_(:,2)-Y50_(1,2))./(Y50_(end,2)-Y50_(1,2));


figure(1)
hold on
plot(Tsim10,Y10_1,'LineWidth',2)
plot(Tsim25,Y25_1,'LineWidth',2)
plot(Tsim50,Y50_1,'LineWidth',2)
legend(['10%';'20%';'50%'])
title('Tank 1: heigts after step responses')
xlabel('time [s]')
ylabel('height [m]')
grid on
hold off
filename = '4_1_1'
saveas(1,fullfile(fname, filename),'png')

figure(2)
hold on
plot(Tsim10,Y10_2,'LineWidth',2)
plot(Tsim25,Y25_2,'LineWidth',2)
plot(Tsim50,Y50_2,'LineWidth',2)
legend(['10%';'20%';'50%'])
title('Tank 2: heigts after step responses')
xlabel('time [s]')
ylabel('height [m]')
grid on
hold off
filename = '4_1_2'
saveas(2,fullfile(fname, filename),'png')



%% Problem 3.2.1 - With low measurement noise
close all;
u0 = [100,200,0,0];

x0 = [0,0,0,0];
T = (0:1:20*60);

N = length(T);

U = u0(1:2).*ones(N,2);
D = zeros(size(U));

R = [0.1 0; 0 0.1];
Lr = chol(R,'lower');
v = Lr*randn(2,N);

[Tsim0,Y0,X0] = FourTankSimulation(T,x0,U',D',v',p);
x0 = X0(end,:)
[Tsim10,Y10,X10] = FourTankSimulation(T,x0,U'*1.1,D',v',p);
[Tsim25,Y25,X25] = FourTankSimulation(T,x0,U'*1.25,D',v',p);
[Tsim50,Y50,X50] = FourTankSimulation(T,x0,U'*1.5,D',v',p);

%Normalizing the two tank levels
Y10_1 = (Y10(:,1)-Y10_(1,1))./(Y10_(end,1)-Y10_(1,1));
Y25_1 = (Y25(:,1)-Y25_(1,1))./(Y25_(end,1)-Y25_(1,1));
Y50_1 = (Y50(:,1)-Y50_(1,1))./(Y50_(end,1)-Y50_(1,1));

Y10_2 = (Y10(:,2)-Y10_(1,2))./(Y10_(end,2)-Y10_(1,2));
Y25_2 = (Y25(:,2)-Y25_(1,2))./(Y25_(end,2)-Y25_(1,2));
Y50_2 = (Y50(:,2)-Y50_(1,2))./(Y50_(end,2)-Y50_(1,2));


figure(3)
hold on
plot(Tsim10,Y10_1,'LineWidth',2)
plot(Tsim25,Y25_1,'LineWidth',2)
plot(Tsim50,Y50_1,'LineWidth',2)
legend(['10%';'20%';'50%'])
title('Tank 1: normalized step responses (low sensor noise)')
xlabel('time [s]')
grid on
hold off
filename = '4_2_1_low'
saveas(3,fullfile(fname, filename),'png')

figure(4)
hold on
plot(Tsim10,Y10_2,'LineWidth',2)
plot(Tsim25,Y25_2,'LineWidth',2)
plot(Tsim50,Y50_2,'LineWidth',2)
legend(['10%';'20%';'50%'])
title('Tank 2: normalized step responses (low sensor noise)')
xlabel('time [s]')
grid on
hold off
filename = '4_2_2_low'
saveas(4,fullfile(fname, filename),'png')

%% Problem 3.2.2  - With medium measurement noise
close all;
u0 = [100,200,0,0];

x0 = [0,0,0,0];
T = (0:1:20*60);

N = length(T);

U = u0(1:2).*ones(N,2);
D = zeros(size(U));

R = [0.2 0; 0 0.2];
Lr = chol(R,'lower');
v = Lr*randn(2,N);

[Tsim0,Y0,X0] = FourTankSimulation(T,x0,U',D',v',p);
x0 = X0(end,:)
[Tsim10,Y10,X10] = FourTankSimulation(T,x0,U'*1.1,D',v',p);
[Tsim25,Y25,X25] = FourTankSimulation(T,x0,U'*1.25,D',v',p);
[Tsim50,Y50,X50] = FourTankSimulation(T,x0,U'*1.5,D',v',p);

%Normalizing the two tank levels
Y10_1 = (Y10(:,1)-Y10_(1,1))./(Y10_(end,1)-Y10_(1,1));
Y25_1 = (Y25(:,1)-Y25_(1,1))./(Y25_(end,1)-Y25_(1,1));
Y50_1 = (Y50(:,1)-Y50_(1,1))./(Y50_(end,1)-Y50_(1,1));

Y10_2 = (Y10(:,2)-Y10_(1,2))./(Y10_(end,2)-Y10_(1,2));
Y25_2 = (Y25(:,2)-Y25_(1,2))./(Y25_(end,2)-Y25_(1,2));
Y50_2 = (Y50(:,2)-Y50_(1,2))./(Y50_(end,2)-Y50_(1,2));

figure(5)
hold on
plot(Tsim10,Y10_1,'LineWidth',2)
plot(Tsim25,Y25_1,'LineWidth',2)
plot(Tsim50,Y50_1,'LineWidth',2)
legend(['10%';'20%';'50%'])
title('Tank 1: normalized step responses (medium sensor noise)')
xlabel('time [s]')
grid on
hold off
filename = '3_2_1_med'
saveas(5,fullfile(fname, filename),'png')

figure(6)
hold on
plot(Tsim10,Y10_2,'LineWidth',2)
plot(Tsim25,Y25_2,'LineWidth',2)
plot(Tsim50,Y50_2,'LineWidth',2)
legend(['10%';'20%';'50%'])
title('Tank 2: normalized step responses (medium sensor noise)')
xlabel('time [s]')
grid on
hold off
filename = '3_2_2_med'
saveas(6,fullfile(fname, filename),'png')

%% Problem 3.2.3  - With high measurement noise

u0 = [100,200,0,0];

x0 = [0,0,0,0];
T = (0:1:20*60);

N = length(T);

U = u0(1:2).*ones(N,2);
D = zeros(size(U));

R = [0.4 0; 0 0.4];
Lr = chol(R,'lower');
v = Lr*randn(2,N);

[Tsim0,Y0,X0] = FourTankSimulation(T,x0,U',D',v',p);
x0 = X0(end,:)
[Tsim10,Y10,X10] = FourTankSimulation(T,x0,U'*1.1,D',v',p);
[Tsim25,Y25,X25] = FourTankSimulation(T,x0,U'*1.25,D',v',p);
[Tsim50,Y50,X50] = FourTankSimulation(T,x0,U'*1.5,D',v',p);


%Normalizing the two tank levels
Y10_1 = (Y10(:,1)-Y10_(1,1))./(Y10_(end,1)-Y10_(1,1));
Y25_1 = (Y25(:,1)-Y25_(1,1))./(Y25_(end,1)-Y25_(1,1));
Y50_1 = (Y50(:,1)-Y50_(1,1))./(Y50_(end,1)-Y50_(1,1));

Y10_2 = (Y10(:,2)-Y10_(1,2))./(Y10_(end,2)-Y10_(1,2));
Y25_2 = (Y25(:,2)-Y25_(1,2))./(Y25_(end,2)-Y25_(1,2));
Y50_2 = (Y50(:,2)-Y50_(1,2))./(Y50_(end,2)-Y50_(1,2));

figure(7)
hold on
plot(Tsim10,Y10_1,'LineWidth',2)
plot(Tsim25,Y25_1,'LineWidth',2)
plot(Tsim50,Y50_1,'LineWidth',2)
legend(['10%';'20%';'50%'])
title('Tank 1: normalized step responses (high sensor noise)')
xlabel('time [s]')
grid on
hold off
filename = '3_2_1_high'
saveas(7,fullfile(fname, filename),'png')

figure(8)
hold on
plot(Tsim10,Y10_2,'LineWidth',2)
plot(Tsim25,Y25_2,'LineWidth',2)
plot(Tsim50,Y50_2,'LineWidth',2)
legend(['10%';'20%';'50%'])
title('Tank 2: heigts after step responses (high sensor noise)')
xlabel('time [s]')
grid on
hold off
filename = '3_2_2_high'
saveas(8,fullfile(fname, filename),'png')

%% 3.4

u0 = [100,200,0,0];

x0 = [0,0,0,0];
T = (0:1:20*60);

N = length(T);

U = u0(1:2).*ones(N,2);
D = zeros(size(U));
v = zeros(N,2);

[Tsim0,Y0,X0] = FourTankSimulation(T,x0,U',D',v,p);
[Tsim10,Y10,X10] = FourTankSimulation(T,x0,U'*1.1,D',v,p);
[Tsim25,Y25,X25] = FourTankSimulation(T,x0,U'*1.25,D',v,p);
[Tsim50,Y50,X50] = FourTankSimulation(T,x0,U'*1.5,D',v,p);

figure(9)
hold on
plot(Tsim0,Y0(:,1)./Y0(end,1))
plot(Tsim10,Y10(:,1)./Y0(end,1))
plot(Tsim25,Y25(:,1)./Y0(end,1))
plot(Tsim50,Y50(:,1)./Y0(end,1))
hold off

figure(10)
hold on
plot(Tsim0,Y0(:,2)./Y0(end,2))
plot(Tsim10,Y10(:,2)./Y0(end,2))
plot(Tsim25,Y25(:,2)./Y0(end,2))
plot(Tsim50,Y50(:,2)./Y0(end,2))
hold off

%% 3.5

clc
close all

u0 = [100,100,0,0];

x0 = [0,0,0,0];
T = (0:1:20*60);

N = length(T);

U = [u0(1).*ones(N,1),zeros(N,1)];
D = zeros(size(U));
v = zeros(N,2);

[Tsim,Y,X] = FourTankSimulation(T,x0,U',D',v,p);

num11 = [0,real(Y(end,1)./u0(1))];
Ynorm = Y(:,1)./Y(end,1);
den11 = [Tsim(find(Ynorm >= 0.632,1,'first')),1];
sim250 = find(Tsim >= 250,1,'first')



transf11 = tf(num11,den11);

subplot(2,2,1);
hold on
plot(Tsim(1:sim250),Y(1:sim250,1)/u0(1),'b','LineWidth',2)
[Ystep,Tstep] = step(transf11,250);
plot(Tstep,Ystep,'--r','LineWidth',2);
title('Input 1 to output 1')
legend('Nonlinear','Transfer function','location','best')
xlabel('Time (seconds)')
ylabel('Height (centimeters)')
xlim([0 250]);
hold off

num12 = [0,real(Y(end,2)./u0(1))];
Ynorm = Y(:,2)./Y(end,2);
den12 = [Tsim(find(Ynorm >= 0.632,1,'first')),1];

transf12 = tf(num12,den12);

subplot(2,2,2);
hold on
plot(Tsim(1:sim250),Y(1:sim250,2)/u0(1),'b','LineWidth',2)
[Ystep,Tstep] = step(transf12,250);
plot(Tstep,Ystep,'--r','LineWidth',2);
title('Input 1 to output 2')
legend('Nonlinear','Transfer function','location','best')
xlabel('Time (seconds)')
ylabel('Height (centimeters)')
xlim([0 250]);
hold off

U = [zeros(N,1),u0(2).*ones(N,1)];

[Tsim,Y,X] = FourTankSimulation(T,x0,U',D',v,p);
sim250 = find(Tsim >= 250,1,'first')


num21 = [0,real(Y(end,1)./u0(2))];
Ynorm = Y(:,1)./Y(end,1);
den21 = [Tsim(find(Ynorm >= 0.632,1,'first')),1];

transf21 = tf(num21,den21);

subplot(2,2,3);
hold on
plot(Tsim(1:sim250),Y(1:sim250,1)/u0(2),'b','LineWidth',2)
[Ystep,Tstep] = step(transf21,250);
plot(Tstep,Ystep,'--r','LineWidth',2);
title('Input 2 to output 1')
legend('Nonlinear','Transfer function','location','best')
xlabel('Time (seconds)')
ylabel('Height (centimeters)')
xlim([0 250]);
hold off

num22 = [0,real(Y(end,2)./u0(2))];
Ynorm = Y(:,2)./Y(end,2);
den22 = [Tsim(find(Ynorm >= 0.632,1,'first')),1];

transf22 = tf(num22,den22);

subplot(2,2,4);
hold on
plot(Tsim(1:sim250),Y(1:sim250,2)/u0(2),'b','LineWidth',2)
[Ystep,Tstep] = step(transf22,250);
plot(Tstep,Ystep,'--r','LineWidth',2);
title('Input 2 to output 2')
legend('Nonlinear','Transfer function','location','best')
xlabel('Time (seconds)')
ylabel('Height (centimeters)')
xlim([0 250]);
hold off



%% 3.6 Estimate noise:

% no fukkin clue



%% 3.7 



%% 3.8




