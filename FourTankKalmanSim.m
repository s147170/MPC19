clc;
clear
close all
addpath(genpath(pwd));
a1 = 1.2272;
a2 = 1.2272;
a3 = 1.2272;
a4 = 1.2272;

A1 = 380.1327;
A2 = 380.1327;
A3 = 380.1327;
A4 = 380.1327;

gamma1 = 0.45;
gamma2 = 0.4;

g = 981;
rho = 1.00;
u0 = [100 200 0 0]

p = [a1;a2;a3;a4;A1;A2;A3;A4;gamma1;gamma2;g;rho];

[A,B,Cy,D,Bv,C,x0] = FourTankLinearized(u0,p)

lambda = eig(A)

tau = 1/(abs(max(lambda)))
%sampling time of 4 makes good sense

Ts = 4


[F,G] = c2d(A,B,Ts)
[F,Gv] = c2d(A,Bv,Ts)


x0 = [0 0 0 0]

R = [1 0; 0 1];
Q = [1 0; 0 1];
N = [1 0; 0 1];


Gv = [0 0;
    0 0;
    1 0;
    0 1;].*rho;

N = 100
u = ones(N,2);

Lr = chol(R,'lower');
v = Lr*randn(2,N); % Sensor noise
v = v';

Lq = chol(Q,'lower'); % Process noise
w = Lq*randn(2,N);
w = w';
N = 100
T = 1:N+1;
%% Function start
close all
X = x0;
X_e = x0;
Y = (C*x0')';
Y_e = (C*x0')';
for k=1:N
    Y_err = Y(end,:) - Y_e(end,:);
    L_err = L*Y_err';
    Xk = F*X(end,:)'+ G*u(k,:)' + Gv*w(k,:)';
    Xk_e = F*X_e(end,:)' + G*u(k,:)' + L_err;
    Yk = C*Xk + v(k,:)';
    Yk_e = C*Xk_e;
    display(Y_err)
    Y = [Y;Yk'];
    Y_e = [Y_e;Yk_e'];
    X = [X;Xk'];
    X_e = [X_e;Xk_e'];
end


plot(T,Y(:,1),T,Y_e(:,1))

