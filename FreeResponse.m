function [Phi,Gama,Gama_d] = FreeResponse(A,B,C,E,N)

[nz,nx]=size(C);
Phi = zeros(N*nz,nx);
T = C;
k1 = 1;
k2 = nz;
for k=1:N
    T = T*A;
    Phi(k1:k2,:) = T;
    k1 = k1+nz;
    k2 = k2+nz;
end
[nt,nu] = size(B)

Gama = zeros(N*nz,N*nu)


k1 = 1;
k2 = nz;
for i=1:N
    T = C * A^i * B
    Gama(k1:k2,1) = T
    k1 = k1+nz;
    k2 = k2+nz;
end

for i = 2:nu:N
    k1 = (N*nz-nz)
    Gama(nz+1:N*nz,i) = Gama(1:k1,i-1);
end

Gama_d = zeros(N*nz,N*nu)
k1 = 1;
k2 = nz;
for i=1:N
    T = C * A^i * E
    Gama_d(k1:k2,1) = T
    k1 = k1+nz;
    k2 = k2+nz;
end

for i = 2:nu:N
    k1 = (N*nz-nz)
    Gama_d(nz+1:N*nz,i) = Gama_d(1:k1,i-1);
end




end


