function [A,B,Cy,D,Bv,C,x0] = FourTankLinearized(u0,p)

    syms m1 m2 m3 m4 h1 h2 h3 h4 rho q1 q1in q2 q2in q3 q3in q4 q4in gamma1 gamma2 F1 F2 F3 F4 a1 a2 a3 a4 A1 A2 A3 A4 grav F10 F20 F30 F40

    % Inflows
    q1in = gamma1*F1;
    q2in = gamma2*F2;
    q3in = (1-gamma2)*F2;
    q4in = (1-gamma1)*F1;

    % Liquid heights
    h1 = m1/(rho*A1);
    h2 = m2/(rho*A2);
    h3 = m3/(rho*A3);
    h4 = m4/(rho*A4);

    % Outflows
    q1 = a1*sqrt(2*grav*h1);
    q2 = a2*sqrt(2*grav*h2);
    q3 = a3*sqrt(2*grav*h3);
    q4 = a4*sqrt(2*grav*h4);

    % 2.1 Develop a deterministic model for the dynamics of the system

    f = [
        rho*(q1in + q3 - q1)
        rho*(q2in + q4 - q2)
        rho*(q3in - q3 + F_3)
        rho*(q4in - q4 + F_4)
    ];

    % 2.2 Develop a mathematical model for the sensors (measurements)

    g = [
        m1/(rho*A1)
        m2/(rho*A2)
    ];

    % 2.3 Develop a mathematical model for the outputs

    h = [
        m1/(rho*A1)
        m2/(rho*A2)
    ];

    a1 = p(1);
    a2 = p(2);
    a3 = p(3);
    a4 = p(4);

    A1 = p(5);
    A2 = p(6);
    A3 = p(7);
    A4 = p(8);

    gamma1 = p(9);
    gamma2 = p(10);

    grav = p(11);
    rho = p(12);

    u = [F1,F2,F3,F4];

    x = [m1,m2,m3,m4];

    [m10,m20,m30,m40] = solve(subs(f,u,u0)==[0 0 0 0],[m1,m2,m3,m4]);
    x0 = [m10,m20,m30,m40];

    A = subs(simplify(jacobian(f,x)),x,x0);

    B = subs(simplify(jacobian(f,[F1,F2])),[F1,F2],[F10,F20]);

    Cy = subs(simplify(jacobian(g,x)),x,x0);

    D = subs(simplify(jacobian(g,[F1,F2])),[F1,F2],[F10,F20]);

    Bv = subs(simplify(jacobian(f,[F3,F4])),[F3,F4],[F30,F40]);

    C = subs(simplify(jacobian(h,x)),x,[m10,m20,m30,m40]);

    A = eval(subs(A));
    B = eval(subs(B));
    Cy = eval(subs(Cy));
    D = eval(subs(D));
    Bv = eval(subs(Bv));
    C = eval(subs(C));
    
    x0 = eval(subs(x0));

end

