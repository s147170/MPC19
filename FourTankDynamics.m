function f_deterministic = FourTankDynamics(t,x,u,d,p)
%Four Tank Dynamics: returns state derivatives for Four Tank System
%     f_deterministic = FourTankDynamics(t,x,u,d,p)
%     x = [m1,m3,m3,m4]: States
%     u = [F1,F2]: Inputs
%     d = [F3,F4]: Disturbances
%     p = [a1,a2,a3,a4,A1,A2,A3,A4,gamma1,gamma2,g,rho]: Parameters

a = p(1:4);
A = p(5:8);
gamma = p(9:10);
g = p(11);
rho = p(12);

F = [u;d];
m = x;

% Inflows
qin = zeros(4,1);
qin(1) = gamma(1)*F(1);
qin(2) = gamma(2)*F(2);
qin(3) = (1-gamma(2))*F(2);
qin(4) = (1-gamma(1))*F(1);

% Liquid heights
h = zeros(4,1);
h(1) = m(1)/(rho*A(1));
h(2) = m(2)/(rho*A(2));
h(3) = m(3)/(rho*A(3));
h(4) = m(4)/(rho*A(4));

% Outflows
qout = zeros(4,1);
qout(1) = a(1)*sqrt(2*g*h(1));
qout(2) = a(2)*sqrt(2*g*h(2));
qout(3) = a(3)*sqrt(2*g*h(3));
qout(4) = a(4)*sqrt(2*g*h(4));

% Dynamics
f_deterministic = [rho*(qin(1) + qout(3) - qout(1));
                   rho*(qin(2) + qout(4) - qout(2));
                   rho*(qin(3) - qout(3) + F(3));
                   rho*(qin(4) - qout(4) + F(4))];
end

